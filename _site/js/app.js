'use strict';


angular.module('BTCWatcher', [
  'ngRoute',
  'BTCWatcher.filters',
  'BTCWatcher.services',
  'BTCWatcher.controllers'
])
.config(['$routeProvider', '$sceDelegateProvider',
    function($routeProvider, $sceDelegateProvider) {
        $sceDelegateProvider.resourceUrlWhitelist([
            'self',
            "blob:http%3A//localhost%3A8000/**",
            "blob:http%3A//192.168.1.57%3A8000/**",
            "blob:https%3A//s3-eu-west-1.amazonaws.com/**"
        ]);

        $routeProvider.when('/watch', {
            templateUrl: 'partials/watch.html',
            controller: 'WatchCtrl'
        });

        $routeProvider.when('/qrscan', {
            templateUrl: 'partials/scan.html',
            controller: 'ScanCtrl'
        });

        $routeProvider.otherwise({redirectTo: '/watch'});
    }]);
