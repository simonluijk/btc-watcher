'use strict';


angular.module('BTCWatcher.controllers', [])
    .controller('WatchCtrl', function($scope, $rootScope, addressStorage, addressBalences) {
        function setAddressesScope(addresses) {
            $scope.addresses = addresses;
            $rootScope.totalBTC = addresses.reduce(function(previous, address) {
                return Math.round((previous + address.balance) * 100000000) / 100000000;
            }, 0);
        }

        setAddressesScope(addressStorage.getAddresses());

        function updateBalences() {
            addressBalences.getBalances($scope.addresses)
                .then(function(addresses) {
                    setAddressesScope(addresses);
                    for (var i = 0; i < addresses.length; i++) {
                        var address = addresses[i];
                        addressStorage.addAddress(address.address,
                                                  address.balance);
                    }
                });
        }

        updateBalences();

        var timer = window.setInterval(updateBalences, 60000);

        $scope.$on("$destroy", function() {
            window.clearInterval(timer);
        });
    })
    .controller('ScanCtrl', function($scope, $location, addressValidator, addressStorage) {
        var localStream,
            timer,
            streaming = false,
            video = document.querySelector('#qr-video'),
            canvas = document.querySelector('#qr-canvas'),
            width = 320, height = 0;

        // Setup video camera
        navigator.getMedia = ( navigator.getUserMedia ||
                               navigator.webkitGetUserMedia ||
                               navigator.mozGetUserMedia ||
                               navigator.msGetUserMedia);

        MediaStreamTrack.getSources(function(sourceInfos) {
            // Choose last video source
            var videoSource = null;
            for (var i = 0; i != sourceInfos.length; ++i) {
                var sourceInfo = sourceInfos[i];
                if (sourceInfo.kind === 'video') {
                    videoSource = sourceInfo.id;
                }
            }

            // Get media stream
            navigator.getMedia({
                video: {
                    optional: [{sourceId: videoSource}]
                },
                optional: {
                    minWidth: 640,
                    minHeight: 360
                }
            }, successCallback, errorCallback);
        });

        function successCallback(stream) {
            localStream = stream;
            $scope.$apply(function(){
                var vendorURL = window.URL || window.webkitURL;
                $scope.videoSRC = vendorURL.createObjectURL(stream);
            });
        }

        function errorCallback(e) {
            console.log('Reeeejected!', e);
        };

        video.addEventListener('canplay', function(event) {
            if (!streaming) {
                height = video.videoHeight / (video.videoWidth / width);
                // video.setAttribute('width', width);
                // video.setAttribute('height', height);
                canvas.setAttribute('width', width);
                canvas.setAttribute('height', height);
                streaming = true;
            }
        }, false);

        qrcode.callback = function(address) {
            try {
                var isValid = addressValidator.validate(address);
            } catch (e) {
                console.log(e)
            }
            if (isValid) {
                addressStorage.addAddress(address);
                $scope.$apply(function() {
                    $location.path('#/watch');
                });
            } else {
                var toast = new fries.Toast({content: "Invalid public address!"});
                console.log("Bad address:", address);
            }
        }

        function processFrame() {
            canvas.getContext('2d').drawImage(video, 0, 0, width, height);
            try {
                qrcode.decode();
            } catch (e) {}
        }

        timer = window.setInterval(processFrame, 300);

        $scope.$on("$destroy", function() {
            window.clearInterval(timer);
            localStream.stop();
        });
    });
