'use strict';


angular.module('BTCWatcher.filters', [])
    .filter('cut', function () {
        return function(value, max, tail) {
            if (!value)
                return '';

            max = parseInt(max, 10);
            if (!max)
                return value;

            if (value.length <= max)
                return value;

            return value.substr(0, max) + (tail || ' …');
        };
    });
