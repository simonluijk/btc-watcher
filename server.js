/*
A simple, nodeJS, http development server that trivializes serving static
files.

To run the server simply place the server.js file in the root of your web
application and issue the command:

$ node server



Mime Types:

You can add to the mimeTypes to serve more file types.

*/

var rootDir = '_site';

var mimeTypes = {
    "html": "text/html",
    "jpeg": "image/jpeg",
    "jpg": "image/jpeg",
    "png": "image/png",
    "gif": "image/gif",
    "js": "text/javascript",
    "coffee": "text/x-coffeescript",
    "json": "application/json",
    "map": "application/json",
    "css": "text/css",
    "ico": "image/x-icon"
};

var http = require("http"),
    url = require("url"),
    path = require("path"),
    fs = require("fs"),
    host = "localhost",
    port = 8000;

if (process.argv[2]) {
    host_str = process.argv[2].split(":")
    host = host_str[0]
    port = host_str[1]
}

http.createServer(function(request, response) {
    var uri = url.parse(request.url).pathname,
        filename = path.join(process.cwd(), rootDir, uri),
        root = uri.split("/")[1],
        virtualDirectory;

    fs.exists(filename, function(exists) {
        if (!exists) {
            response.writeHead(404, {"Content-Type": "text/plain"});
            response.write("404 Not Found\n");
            response.end();
            console.error('404: ' + uri);
            return;
        }

        if (fs.statSync(filename).isDirectory()) {
            filename = path.join(filename, 'index.html');
        }

        fs.readFile(filename, "binary", function(err, file) {
            if(err) {
                response.writeHead(500, {"Content-Type": "text/plain"});
                response.write(err + "\n");
                response.end();
                console.error('500: ' + uri);
                return;
            }

            var mimeType = mimeTypes[path.extname(filename).split(".")[1]];
            response.writeHead(200, {"Content-Type": mimeType});
            response.write(file, "binary");
            response.end();
            console.log('200: ' + uri + ' as ' + mimeType);
        });
    });
}).listen(parseInt(port, 10), host);

console.log("Static file server running at http://" + host + ":" + port);
