module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    coffee: {
      dist: {
        options: {
          join: false,
          nojoin: true,
          sourceMap: true
        },
        files: [{
          expand: true,
          cwd: '_site/coffee/',
          src: ['**/*.coffee'],
          dest: '_site/js/',
          ext: '.js',
        }]
      }
    },

    less: {
      dist: {
        options: {
          compress: true
        },
        files: {
          '_site/css/main.min.css': '_site/less/main.less'
        }
      }
    },

    copy: {
      less: {
        files: [
          {
            expand: true,
            src: ['_site/bower_components/bootstrap/dist/fonts/*'],
            dest: '_site/img/fonts/',
            flatten: true,
            filter: 'isFile'
          }
        ]
      }
    },

    watch: {
      less: {
        files: [
          '_site/less/*.less',
          '_site/less/*/*.less',
          'Gruntfile.js'
        ],
        tasks: [
          'less',
          'copy'
        ]
      },
      coffee: {
        files: [
          '_site/coffee/*.coffee',
          'Gruntfile.js'
        ],
        tasks: [
          'coffee',
        ]
      }
    }

  });

  grunt.loadNpmTasks('grunt-contrib-coffee');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-copy');

  grunt.registerTask('default', ['build']);
  grunt.registerTask('build', [
    'coffee',
    'less',
    'copy'
  ]);

};
